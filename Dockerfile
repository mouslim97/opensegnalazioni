# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.8-slim-buster

#EXPOSE 5000

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install pip requirements
COPY requirements.txt .
RUN python -m pip install -r requirements.txt

WORKDIR /app

RUN mkdir src
RUN mkdir src/data
RUN mkdir src/models
RUN mkdir src/models/model_2

#COPY . /app
COPY /src/opensegnalazioni.py /app/src 
COPY /src/data/ITGivenFemale.json /app/src/data
COPY /src/data/ITGivenMale.json /app/src/data
COPY /src/data/cognomi.txt /app/src/data
COPY /src/data/Elenco-comuni-italiani.csv /app/src/data
COPY /src/data/vie_genova.json /app/src/data
COPY /src/models/model_2/cat_encoding_close.json /app/src/models/model_2
COPY /src/models/model_2/parent_cat_encoding_close.json /app/src/models/model_2
COPY /src/models/model_2/vectorizer_micro_cat.pk /app/src/models/model_2
COPY /src/models/model_2/vectorizer_macro_cat.pk /app/src/models/model_2
COPY /src/models/model_2/model_micro_cat.joblib /app/src/models/model_2
COPY /src/models/model_2/model_macro_cat.joblib /app/src/models/model_2



# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-python-configure-containers
#RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
#USER appuser

ENV FLASK_APP=src.opensegnalazioni:app

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
#CMD ["gunicorn", "--bind", "0.0.0.0:5000", "src.opensegnalazioni:app"]
#CMD ["python3", "src/opensegnalazioni.py"]
CMD ["flask", "run", "--host", "0.0.0.0"]
