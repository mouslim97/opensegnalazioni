import json
from tqdm import tqdm

NUMBER_OF_PAGES = 623
ITEMS_PER_PAGE = 10
complaints = []

with open('src/data/segnalazioni.json') as f:
  posts_data = json.load(f)

for i in tqdm(range(NUMBER_OF_PAGES-1)):
  for j in range(ITEMS_PER_PAGE):
    if(len(posts_data[i]['items'][j]['_embedded']['categories']) != 0):
      post = {
        "subject": posts_data[i]['items'][j]['subject'], 
        "description": posts_data[i]['items'][j]['description'], 
        "categoryId": posts_data[i]['items'][j]['_embedded']['categories'][0]['id'],
        "parentCategoryId": posts_data[i]['items'][j]['_embedded']['categories'][0]['parent'],
        "categoryName": posts_data[i]['items'][j]['_embedded']['categories'][0]['name'],
        "status": posts_data[i]['items'][j]['status'] 
      }
      complaints.append(post)

with open("segnalazioni_dataset.json",'w') as f:
    json.dump(complaints, f)