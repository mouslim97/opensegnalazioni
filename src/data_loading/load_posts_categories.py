import requests
import json
from tqdm import tqdm

###############################CATEGORIES###################################
NUMBER_OF_PAGES = 622
ITEMS_PER_PAGE = 10

with open('src/segnalazioni.json') as f:
  data = json.load(f)

url_posts_categories = "https://segnalazioni.comune.genova.it/api/sensor/posts/"

posts_categories = []
    
for i in tqdm(range(NUMBER_OF_PAGES-1)):
    for j in range(ITEMS_PER_PAGE):
        resp = requests.get(url_posts_categories + str(data[i]['items'][j]['id']) + "/categories", headers={'Authorization': 'Basic bW91c2xpbTk3OkIsRiw5LDEsNyxKLHku'})
        json_posts_categories_data = resp.json()
        posts_categories.append(json_posts_categories_data)

print(len(posts_categories))

with open('categoria_segnalazioni.json', 'w') as outfile:
    json.dump(posts_categories, outfile)