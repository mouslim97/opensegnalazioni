import requests
import json
from tqdm import tqdm

###############################POSTS###################################
results = []

url_posts = "https://segnalazioni.comune.genova.it/api/sensor/posts?limit=10&cursor=%2A&embed=categories,owners"

response = requests.get(url_posts, headers={'Authorization': 'Basic bW91c2xpbTk3OkIsRiw5LDEsNyxKLHku'}, timeout=20)
json_posts_data = response.json()
results.append(json_posts_data)

pbar = tqdm(total=100)
while json_posts_data['next']:
    response = requests.get(json_posts_data['next'] + "&embed=categories,owners", headers={'Authorization': 'Basic bW91c2xpbTk3OkIsRiw5LDEsNyxKLHku'}, timeout=20)
    json_posts_data = response.json()
    results.append(json_posts_data)
    pbar.update(5)
pbar.close()

print(len(results))

with open('segnalazioni.json', 'w') as outfile:
    json.dump(results, outfile)
