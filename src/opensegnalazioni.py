from flask import Flask
from flask_restful import Resource, Api, reqparse
from ordered_set import OrderedSet
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords 
from nltk.stem.porter import PorterStemmer
from autocorrect import Speller
from simplemma import text_lemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer
from joblib import load
import os
import pandas as pd
import numpy as np
import ast
import re
import nltk
import simplemma
import json
import pickle

app = Flask(__name__)
api = Api(app)
top_k = 3
top_parent_k = 3
#path = '/home/mouslim/Scrivania/opensegnalazioni/src'
nltk.download('stopwords')
nltk.download('punkt')

def clean_document(document):
    stop_words_set = stopwords.words('italian')
    stemmer = PorterStemmer()
    spell = Speller(lang='it')
    langdata = simplemma.load_data('it')
    with open(os.getcwd() + '/src/data/ITGivenFemale.json') as f:
        data_female = json.load(f)

    with open(os.getcwd() + '/src/data/ITGivenMale.json') as f:
        data_male = json.load(f)

    data_names = list()
    for i in range(len(data_female)):
        data_names.append(str(data_female[i]['name']))
    for i in range(len(data_male)):
        data_names.append(str(data_male[i]['name']))
    for i in range(len(data_names)):
        data_names[i] = data_names[i].lower()
    #data_names = OrderedSet(data_names)

    df_cognomi = pd.read_csv(os.getcwd() + '/src/data/cognomi.txt')
    df_cognomi['Cognome'] = df_cognomi['Cognome'].str.lower()
    df_cognomi = list(df_cognomi['Cognome'])
    #df_cognomi = OrderedSet(df_cognomi['Cognome'])

    df_comuni = pd.read_csv(os.getcwd() + '/src/data/Elenco-comuni-italiani.csv', sep=';', encoding='latin1')
    df_comuni['Denominazione in italiano'] = df_comuni['Denominazione in italiano'].str.lower()
    df_comuni = list(df_comuni['Denominazione in italiano'])
    #df_comuni = OrderedSet(df_comuni['Denominazione in italiano'])

    with open(os.getcwd() + '/src/data/vie_genova.json') as f:
        data_streets = json.load(f)
    df_streets = pd.DataFrame(data_streets['Risposta']['Strada'])
    df_streets['NOME_VIA'] = df_streets['NOME_VIA'].str.lower()
    corpus = ""
    for item in df_streets:
        corpus = corpus + " " + item
    corpus = OrderedSet(word_tokenize(corpus))
    corpus = list(corpus)
    df_streets = corpus
    #df_streets = OrderedSet(df_streets['NOME_VIA'])

    document = re.sub('[^A-Za-z]', ' ', document)
    document = document.lower()

    tokenized_text = word_tokenize(document)
    tokenized_text = [w for w in tokenized_text if not w in stop_words_set]
    tokenized_text = [w for w in tokenized_text if not w in data_names]
    tokenized_text = [w for w in tokenized_text if not w in df_comuni]
    tokenized_text = [w for w in tokenized_text if not w in df_cognomi]
    tokenized_text = [w for w in tokenized_text if not w in df_streets]
    tokenized_text = [w for w in tokenized_text if not w in ['copia']]
    
    words = []
    for word in tokenized_text:
        tmp = " ".join(text_lemmatizer(word, langdata))
        tmp = stemmer.stem(tmp)
        tmp = stemmer.stem(spell(tmp))
        words.append(tmp)
    
    cleaned_text = " ".join(words)
    document = cleaned_text
    return document

def extract_features(text, vectorizer_path):
    tfidf_vectorizer = pd.read_pickle(vectorizer_path)
    data = pd.DataFrame({'subject_description':[text]})
    text_vectorized = tfidf_vectorizer.transform(data['subject_description'])
    return text_vectorized

def get_top_k_predictions(model,text,k):
    probs = model.predict_proba(text)
    best_n = np.argsort(probs, axis=1)[:,-k:]
    preds=[[model.classes_[predicted_cat] for predicted_cat in prediction] for prediction in best_n]
    preds=[item[::-1] for item in preds]
    return preds

def get_pred_cats(preds):
    pred_cats = pd.DataFrame(columns=['categoryId','categoryName'])
    
    with open(os.getcwd() + '/src/models/model_2/cat_encoding_close.json') as f:
        cat_encoding = json.load(f)
    cat_encoding_df = pd.DataFrame(cat_encoding)
    
    for item in preds:
        for cat in item:
            pred_cats = pred_cats.append({
                'categoryId': int(cat_encoding_df['categoryId'][cat_encoding_df['label'] == cat]),
                'categoryName': cat_encoding_df['categoryName'][cat_encoding_df['label'] == cat].values[0]
            }, ignore_index=True)    
    return pred_cats

def get_pred_parent_cats(preds):
    pred_cats = pd.DataFrame(columns=['parentCategoryId','parentCategoryName'])
    
    with open(os.getcwd() + '/src/models/model_2/parent_cat_encoding_close.json') as f:
        cat_encoding = json.load(f)
    cat_encoding_df = pd.DataFrame(cat_encoding)
    
    for item in preds:
        for cat in item:
            pred_cats = pred_cats.append({
                'parentCategoryId': int(cat_encoding_df['parentCategoryId'][cat_encoding_df['parent_label'] == cat].unique()),
                'parentCategoryName': cat_encoding_df['parentCategoryName'][cat_encoding_df['parent_label'] == cat].values[0]
            }, ignore_index=True)    
    return pred_cats

class CategoryHints(Resource):

    def post(self):
        parser = reqparse.RequestParser()  # initialize
        parser.add_argument('subject', required=True)  # add args
        parser.add_argument('description', required=True) 
        args = parser.parse_args()  # parse arguments to dictionary

        subject = args['subject']
        description = args['description']

        text = subject + " " + description
        text = clean_document(text)
        text = extract_features(text, os.getcwd() + '/src/models/model_2/vectorizer_micro_cat.pk')
        model = load(os.getcwd() + '/src/models/model_2/model_micro_cat.joblib')
        preds = get_top_k_predictions(model,text,top_k)
        pred_cats = get_pred_cats(preds)

        return {'pred_cats': pred_cats.to_dict(orient='records')}, 200  # return data with 200 OK

class parentCategoryHints(Resource):

    def post(self):
        parser = reqparse.RequestParser()  # initialize
        parser.add_argument('subject', required=True)  # add args
        parser.add_argument('description', required=True) 
        args = parser.parse_args()  # parse arguments to dictionary

        subject = args['subject']
        description = args['description']

        text = subject + " " + description
        text = clean_document(text)
        text = extract_features(text, os.getcwd() + '/src/models/model_2/vectorizer_macro_cat.pk')
        model = load(os.getcwd() + '/src/models/model_2/model_macro_cat.joblib')
        preds = get_top_k_predictions(model,text,top_parent_k)
        pred_cats = get_pred_parent_cats(preds)

        return {'pred_cats': pred_cats.to_dict(orient='records')}, 200  # return data with 200 OK

api.add_resource(CategoryHints, '/categoryhints/micro')  # add endpoints
api.add_resource(parentCategoryHints, '/categoryhints/macro')  # add endpoints

if __name__ == '__main__':
    app.run()  # run our Flask app